# Espresso local:

To build the apks & activate the plugin - Open a command-line/terminal window in the project's folder and run the following tasks in gradle:</br></br>
`gradle assembleDebug assembleAndroidTest perfecto-android-inst`</br>

# Espresso CI (Jenkins):
To perform and entire build, deploy and test in perfecto, build using the below gradle tasks inside invoke gradle script:</br></br>
`assembleDebug 
assembleAndroidTest
perfecto-android-inst 
-PjobName=${JOB_NAME} -PjobNumber=${BUILD_NUMBER} -PconfigFileLocation=configFile.json -PsecurityToken=${securityToken}`</br>